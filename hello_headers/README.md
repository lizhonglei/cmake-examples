# hello_headers
- 可用变量

| Variable | Info |
| -------------------------- | ---- |
| CMAKE_SOURCE_DIR           | cmake source dir, 根源代码目录，暂认为就是 project source dir |
| PROJECT_SOURCE_DIR         | project source dir, 工程顶层目录  |
| CMAKE_CURRENT_SOURCE_DIR   | cmake current source dir, 当前处理的 CMakeLists.txt 所在路径 |
| CMAKE_CURRENT_BINARY_DIR   | cmake current binary dir, 当前 build 目录|
| CMAKE_BINARY_DIR           | cmake binary dir, 运行 cmake 的目录 |
| PROJECT_BINARY_DIR         | project binary dir, 暂认为是 cmake binary dir |

- 源文件变量
不建议对源文件使用变量，通常直接明确列出所有源文件。

- 包含目录
```cmake
target_include_directories(target 
    PRIVATE
        ${PROJECT_SOURCE_DIR}/include
)
```

- make VERBOSE=1 可详细输出日志

