﻿/* 声明 Hello 类，Hello 的方法是 print(). */

#ifndef __HELLO_H
#define __HELLO_H


class Hello
{
public:
	void print();
};


#endif // __HELLO_H