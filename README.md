# cmake-examples

## 1 介绍
- cmake 学习 
    - 原始示例：https://github.com/ttroy50/cmake-examples。
    - 注解：https://sfumecjf.github.io/cmake-examples-Chinese/

## 2 make、automake 和 cmake
make 的流行带动一批自动生成 makefile 的工具，进一步减轻项目构建中的工作量。
- make 调用 makefile 的规则，执行编译命令
- automake 是工具集 autotools 中的一员，需要配合其中的其他工具使用：autoscan、aclocal、autoconf 和 autoheader。大概需要 9 步来生成一个 makefile。
- cmake 将 automake 压缩到只需要 2 步：
    - 编写 CMakeLists.txt
    - 执行 cmake
